# PHP Utilities

A small library of PHP utility classes.

## Installation

To install this library, add it as a Composer dependency:

```json
 {
   "require": {
      "americanreading/utilities": "^1"
   }
}
```

## Development

Use Docker Compose to run `phpunit` and `composer` locally.  

```bash
# Build images
docker-compose build

# Install Composer dependencies
docker-compose run --rm php composer install

# Start the services
docker-compose up -d
```

## Unit Tests

Use PHPUnit in the `php` service to run unit tests.

```
docker-compose run --rm php phpunit
```
