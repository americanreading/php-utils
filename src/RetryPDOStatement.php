<?php

namespace AmericanReading\Util;

use PDO;
use PDOException;
use PDOStatement;

/**
 * A PDOStatement that retries statements failed due to deadlock
 */
class RetryPDOStatement extends PDOStatement
{
    /** @var PDO */
    protected $pdo;

    /** @var int How many ms to wait between retries. */
    const DEADLOCK_RETRY_MS = 150;

    /**
     * RetryPDOStatement constructor.
     *
     * @param PDO $pdo
     */
    protected function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Executes a prepared statement
     *
     * @link   http://php.net/manual/en/pdostatement.execute.php
     * @param array $input_parameters [optional] An array of values with as many elements as there
     * are bound parameters in the SQL statement being executed. All values are treated as
     * PDO::PARAM_STR.
     * @throws PDOException
     * @return bool TRUE on success or FALSE on failure.
     */
    public function execute(?array $input_parameters = null): bool
    {
        $ex = null;
        $retries = 3;
        while ($retries-- > 0) {
            $ex = null;
            try {
                $result = parent::execute($input_parameters);
                $retries = 0;
            } catch (PDOException $e) {
                $ex = $e;
                if (str_starts_with($e->getMessage(), "SQLSTATE[40001]: Serialization failure: 1213 Deadlock")) {
                    // As an improvement this timing could possibly be made slightly random, to avoid failures in phase with
                    // each other (i.e. all competing tx retrying at the same time). But that is probably overkill.
                    usleep(self::DEADLOCK_RETRY_MS * 1000);     // Remember - usleep is microseconds (μs), not ms
                } else {
                    $retries = 0;
                }
            }
        }

        if ($this->pdo->getAttribute(PDO::ATTR_ERRMODE) !== PDO::ERRMODE_EXCEPTION && $result === false) {
            $error = $this->errorInfo();
            $ex = new PDOException($error[2], (int)$error[0]);
        }

        if ($this->pdo->getAttribute(PDO::ATTR_ERRMODE) === PDO::ERRMODE_EXCEPTION && $ex !== null) {
            throw $ex;
        }
        return $result;
    }
}

