<?php

namespace AmericanReading\Util;

class EnvironmentReader
{
    public function get(string $varname, string $default = ''): string
    {
        $value = getenv($varname);
        if ($value === false) {
            return $default;
        }
        return $value;
    }

    public function getBool(string $varname, bool $default = false): bool
    {
        $value = getenv($varname);
        if ($value === false) {
            return $default;
        }
        $value = strtolower($value);
        $trueValues = ['1', 'on', 't', 'true', 'y', 'yes'];
        return in_array($value, $trueValues);
    }

    public function getInt(string $varname, int $default = 0): int
    {
        $value = getenv($varname);
        if ($value === false) {
            return $default;
        }
        return (int) $value;
    }
}
