<?php

namespace AmericanReading\Util;

use PDO;

class RetryDatabaseConnectionProvider extends DatabaseConnectionProvider
{
    public function getConnection(): PDO
    {
        $connection = parent::getConnection();
        if ($connection !== null) {
            $connection->setAttribute(PDO::ATTR_STATEMENT_CLASS, [RetryPDOStatement::class, [$connection]]);
        }
        return $connection;
    }
}
