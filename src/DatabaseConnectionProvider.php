<?php

namespace AmericanReading\Util;

use PDO;

class DatabaseConnectionProvider
{
    /** @var string */
    private $dsn;
    /** @var string */
    private $username;
    /** @var string */
    private $password;
    /** @var array */
    private $options;
    /** @var PDO */
    private $connection = null;

    public function __construct(
        string $dsn,
        string $username,
        string $password,
        array $options
    ) {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
    }

    public function getConnection(): PDO
    {
        if ($this->connection === null) {
            $this->connection = new PDO(
                $this->dsn, $this->username, $this->password, $this->options);
        }
        return $this->connection;
    }

    public function unsetConnection()
    {
        $this->connection = null;
    }
}
