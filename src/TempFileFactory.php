<?php

namespace AmericanReading\Util;

class TempFileFactory
{
    public function create(): string
    {
        return tempnam(sys_get_temp_dir(), 'tmp');
    }
}
