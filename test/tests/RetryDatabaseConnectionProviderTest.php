<?php

namespace AmericanReading\Test\Util;

use AmericanReading\Util\DatabaseConnectionProvider;
use AmericanReading\Util\RetryDatabaseConnectionProvider;
use PDO;
use PHPUnit\Framework\TestCase;

class RetryDatabaseConnectionProviderTest extends DatabaseConnectionProviderTest
{
    // These constants correspond to the configuration in docker-compose.yml
    const DSN = 'mysql:host=mysql;dbname=test';
    const USERNAME = 'root';
    const PASSWORD = 'test';

    protected function getDbProvider()
    {
        return new RetryDatabaseConnectionProvider(
            self::DSN,
            self::USERNAME,
            self::PASSWORD,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false
            ]
        );
    }

}
