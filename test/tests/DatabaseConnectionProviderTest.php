<?php

namespace AmericanReading\Test\Util;

use AmericanReading\Util\DatabaseConnectionProvider;
use PDO;
use PHPUnit\Framework\TestCase;

class DatabaseConnectionProviderTest extends TestCase
{
    // These constants correspond to the configuration in docker-compose.yml
    const DSN = 'mysql:host=mysql;dbname=test';
    const USERNAME = 'root';
    const PASSWORD = 'test';

    protected function getDbProvider()
    {
        return new DatabaseConnectionProvider(
            self::DSN,
            self::USERNAME,
            self::PASSWORD,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false
            ]
        );
    }

    public function testProvidesPdoConnection()
    {
        $provider = $this->getDbProvider();
        $db = $provider->getConnection();
        $this->assertInstanceOf('PDO', $db);
    }

    public function testProvidesSameConnectionOnMultipleRequests()
    {
        $provider = $this->getDbProvider();
        $db1 = $provider->getConnection();
        $db2 = $provider->getConnection();
        $this->assertSame($db1, $db2);
    }

    public function testProvidesNewConnectionAfterUnsettingConnection()
    {
        $provider = $this->getDbProvider();
        $db1 = $provider->getConnection();
        $provider->unsetConnection();
        $db2 = $provider->getConnection();
        $this->assertNotSame($db1, $db2);
    }
}
