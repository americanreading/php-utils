<?php

namespace AmericanReading\Test\Util;

use AmericanReading\Util\TempFileFactory;
use PHPUnit\Framework\TestCase;

class TempFileFactoryTest extends TestCase
{
    /** @var TempFileFactory */
    private $factory;

    public function setUp(): void
    {
        $this->factory = new TempFileFactory();
    }

    // -------------------------------------------------------------------------

    public function testProvidesWritablePath()
    {
        $text = 'This is some text';
        $path = $this->factory->create();
        file_put_contents($path, $text);
        $readText = file_get_contents($path);
        $this->assertEquals($text, $readText);
        unlink($path);
    }

    public function testProvidesUniquePath()
    {
        $paths = [
            $this->factory->create(),
            $this->factory->create(),
            $this->factory->create(),
            $this->factory->create(),
            $this->factory->create()
        ];
        $uniquePaths = array_unique($paths);
        $this->assertEquals(count($paths), count($uniquePaths));
        foreach ($paths as $path) {
            unlink ($path);
        }
    }
}
