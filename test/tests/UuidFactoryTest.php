<?php

namespace AmericanReading\Util\Test;

use AmericanReading\Util\UuidFactory;
use PHPUnit\Framework\TestCase;

class UuidFactoryTest extends TestCase
{
    private $factory;

    public function setUp(): void
    {
        parent::setUp();
        $this->factory = new UuidFactory();
    }

    public function testGeneratesUniqueValues()
    {
        $count = 10000;
        $ids = [];
        for ($i = 0; $i < $count; ++$i) {
            $ids[] = $this->factory->create();
        }
        $uniqueIds = array_unique($ids);
        $this->assertEquals($count, count($uniqueIds));
    }
}
