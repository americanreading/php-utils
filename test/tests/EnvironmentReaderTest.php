<?php

namespace AmericanReading\Util\Test;

use AmericanReading\Util\EnvironmentReader;
use PHPUnit\Framework\TestCase;

class EnvironmentReaderTest extends TestCase
{
    const ENV = 'TEST_VAR';

    private $env;

    public function setUp(): void
    {
        parent::setUp();
        putenv(self::ENV);
        $this->env = new EnvironmentReader();
    }

    private function put($value): void
    {
        putenv(self::ENV . '=' . $value);
    }

    // -------------------------------------------------------------------------
    // String

    public function testProvidesStringValueWhenSet()
    {
        $value = 'My string';
        $this->put($value);
        $this->assertSame($value, $this->env->get(self::ENV));
    }

    public function testProvidesDefaultStringValueWhenNotSet()
    {
        $value = 'My default string';
        $this->assertSame($value, $this->env->get(self::ENV, $value));
    }

    // -------------------------------------------------------------------------
    // Bool

    /** @dataProvider booleanProvider */
    public function testProvidesBooleanValuesWhenSet($expected, $value)
    {
        $this->put($value);
        $this->assertSame($expected, $this->env->getBool(self::ENV));
    }

    public function booleanProvider()
    {
        return [
            [true, '1'],
            [true, 't'],
            [true, 'True'],
            [true, 'y'],
            [true, 'on'],
            [true, 'YES'],
            [false, '0'],
            [false, 'NO'],
            [false, 'banana']
        ];
    }

    public function testProvidesDefaultBooleanWhenNotSet()
    {
        $this->assertTrue($this->env->getBool(self::ENV, true));
        $this->assertFalse($this->env->getBool(self::ENV, false));
    }

    // -------------------------------------------------------------------------
    // Int

    /** @dataProvider intProvider */
    public function testProvidesIntValuesWhenSet($expected, $value)
    {
        $this->put($value);
        $this->assertSame($expected, $this->env->getInt(self::ENV, 0));
    }

    public function intProvider()
    {
        return [
            [1, '1'],
            [2, '000002'],
            [3, '    3 '],
            [4, '4.12'],
            [0, 'banana'],
            [-1, '-1']
        ];
    }

    public function testProvidesDefaultIntWhenNotSet()
    {
        $this->assertSame(17, $this->env->getInt(self::ENV, 17));
    }
}
