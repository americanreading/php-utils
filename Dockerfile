FROM php:8.1

RUN apt-get update \
  && apt-get -y install \
    git \
    unzip \
    wget \
    zip \
  && apt-get clean

# Download and install Composer
COPY ./install-composer.sh /tmp/install-composer.sh
RUN chmod +x /tmp/install-composer.sh; sync && \
  /tmp/install-composer.sh && \
  rm /tmp/install-composer.sh

# Install XDebug
RUN pecl install xdebug \
  && docker-php-ext-enable xdebug

# Add symlink for phpunit for easier running
RUN ln -s /usr/local/src/util/vendor/bin/phpunit /usr/local/bin/phpunit

# PDO MySQL
RUN docker-php-ext-install pdo_mysql

# Create a user
RUN useradd -ms /bin/bash arc
USER arc

WORKDIR /usr/local/src/util
